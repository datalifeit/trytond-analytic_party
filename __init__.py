# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import account
from . import configuration
from . import sale
from . import purchase
from . import invoice


def register():
    Pool.register(
        party.Party,
        party.PartyAnalyticAccount,
        account.Account,
        configuration.Configuration,
        configuration.ConfigurationForceCode,
        module='analytic_party', type_='model')
    Pool.register(
        sale.SaleLine,
        module='analytic_party', type_='model',
        depends=['analytic_sale'])
    Pool.register(
        purchase.PurchaseLine,
        module='analytic_party', type_='model',
        depends=['analytic_purchase'])
    Pool.register(
        invoice.InvoiceLine,
        module='analytic_party', type_='model',
        depends=['analytic_invoice'])
