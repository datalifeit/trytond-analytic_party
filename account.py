# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Account(metaclass=PoolMeta):

    __name__ = 'analytic_account.account'

    parties = fields.Many2Many(
        'party.party-analytic_account.account', 'account', 'party',
        'Parties')

    @classmethod
    def validate(cls, accounts):
        super().validate(accounts)
        cls.check_code_numeric(accounts)

    @classmethod
    def check_code_numeric(cls, accounts):
        pool = Pool()
        Conf = pool.get('party.configuration')

        if Conf(1).force_analytic_code_numeric:
            for account in accounts:
                if (account.code and account.parties
                        and not account.code.isdigit()):
                    raise UserError(gettext('analytic_party.'
                        'msg_invalid_analytic_account_code',
                        account=account.rec_name))
