# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelSQL


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    analytic_accounts = fields.Many2Many(
        'party.party-analytic_account.account', 'party', 'account',
        'Analytic Accounts',
        domain=[
            ('type', '=', 'normal')])

    @classmethod
    def validate(cls, parties):
        super().validate(parties)
        cls._check_analytic_account_code_numeric(parties)

    @classmethod
    def _check_analytic_account_code_numeric(cls, parties):
        pool = Pool()
        Conf = pool.get('party.configuration')
        Account = pool.get('analytic_account.account')

        if Conf(1).force_analytic_code_numeric:
            Account.check_code_numeric([a for party in parties
                for a in party.analytic_accounts])


class PartyAnalyticAccount(ModelSQL):
    '''Party - Analytic Account'''
    __name__ = 'party.party-analytic_account.account'

    party = fields.Many2One('party.party', 'Party', select=True, required=True,
        ondelete='CASCADE')
    account = fields.Many2One('analytic_account.account',
        'Account', select=True, required=True, ondelete='RESTRICT')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.__access__.add('account')
