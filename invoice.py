# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('invoice', 'analytic_accounts', methods=[
        'get_party_analytic_account_domain'])
    def on_change_invoice(self):
        pool = Pool()
        AnalyticAccount = pool.get('analytic_account.account')

        try:
            super().on_change_invoice()
        except AttributeError:
            pass
        if self.invoice:
            for entry_acc in self.analytic_accounts:
                accounts = AnalyticAccount.search(
                    self.get_party_analytic_account_domain(entry_acc.root))
                if accounts and not getattr(entry_acc, 'account', None):
                    entry_acc.account = accounts[0]

    @fields.depends('invoice', '_parent_invoice.company',
        '_parent_invoice.party')
    def get_party_analytic_account_domain(self, root):
        return [
            ('root', '=', root),
            ('company', '=', self.invoice.company),
            ('parties', '=', self.invoice.party.id)
        ]
