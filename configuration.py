# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import ModelSQL, fields
from trytond.modules.company.model import CompanyValueMixin


class Configuration(metaclass=PoolMeta):
    __name__ = 'party.configuration'

    force_analytic_code_numeric = fields.MultiValue(
        fields.Boolean('Force analytic code numeric'))


class ConfigurationForceCode(ModelSQL, CompanyValueMixin):
    '''Party configuration force analytic code numeric'''
    __name__ = 'party.configuration.force_analytic_code_numeric'

    force_analytic_code_numeric = fields.Boolean('Force analytic code numeric')
