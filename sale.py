# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('sale', 'analytic_accounts', methods=[
        'get_party_analytic_account_domain'])
    def on_change_sale(self):
        pool = Pool()
        AnalyticAccount = pool.get('analytic_account.account')

        try:
            super().on_change_sale()
        except AttributeError:
            pass
        if self.sale:
            for entry_acc in self.analytic_accounts:
                accounts = AnalyticAccount.search(
                    self.get_party_analytic_account_domain(entry_acc.root))
                if accounts:
                    entry_acc.account = accounts[0]

    @fields.depends('sale', '_parent_sale.company', '_parent_sale.party')
    def get_party_analytic_account_domain(self, root):
        return [
            ('root', '=', root),
            ('company', '=', self.sale.company),
            ('parties', '=', self.sale.party.id)
        ]
